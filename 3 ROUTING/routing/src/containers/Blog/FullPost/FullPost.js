import React, { Component } from 'react';
import axios from 'axios';

import './FullPost.scss';

class FullPost extends Component {
    state = {
        loadedPost: null,
        errors: {
            getError: false,
            deleteError: false
        }
    };

    componentDidMount() {
       this.loadPosts();
    }

    componentDidUpdate() {
        this.loadPosts();
    }

    loadPosts() {
        if(this.state.errors.getError)
        return;
    
        if(this.props.match.params.id) {
            if(!this.state.loadedPost || +this.props.match.params.id !== this.state.loadedPost.id) {
                axios.get(`/posts/${this.props.match.params.id}`)
                    .then(response => {
                        this.setState({
                            loadedPost: response.data
                        });
                    }).catch(error => {
                        console.log(error);
                        
                        this.setState(state => {
                            return {
                                errors: {
                                    ...state.errors,
                                    getError: true
                                }
                            }
                        })
                    });
            }
        }
    }

    handlePostDelete = () => {
        axios.delete(`/posts/${this.props.match.params.id}`)
            .then(response => {
                console.log(response);
            }).catch(error => {
                console.log(error);
                
                this.setState(state => {
                    return {
                        errors: {
                            ...state.errors,
                            deleteError: true
                        }
                    }
                })
            });
    }

    render () {
        if(this.state.errors.getError)
            return <p className="full-post__error">Something went wrong! Cannot get the post.</p>;

        if(this.state.errors.deleteError)
            return <p className="full-post__error">Something went wrong! The post cannot be deleted.</p>;

        let post = <p className="full-post__initial-txt">Please select a Post!</p>;

        if(this.props.match.params.id)
            post = <p className="full-post__initial-txt">Loading...</p>;

        if(this.state.loadedPost) {
            post = (
                <div className="full-post">
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                    <div className="edit">
                        <button 
                            className="edit__btn edit__btn--delete"
                            onClick={this.handlePostDelete}>Delete</button>
                    </div>
                </div>
            );
        }
        
        return post;
    }
}

export default FullPost;