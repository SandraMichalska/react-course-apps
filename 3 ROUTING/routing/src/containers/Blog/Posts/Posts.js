
import React from 'react';
import axios from '../../../axios';
import Post from '../../../components/Post/Post';
import FullPost from '../FullPost/FullPost';
import './Posts.scss';
import { Link, Route } from 'react-router-dom';


class Posts extends React.Component {
    state = {
        posts: [],
        // error: false
    };

    componentDidMount() {
        // see route-related props: match, location, history
        console.log(this.props);
        
        axios.get('/posts')
            .then(response => {
                const somePosts = response.data.slice(0, 6);
                const updatedPosts = somePosts.map(post => {
                    return {
                        ...post,
                        author: "Sandra"
                    }
                });

                this.setState({
                    posts: updatedPosts
                })
            }).catch(error => {
                console.log(error);
                
                // this.setState({
                //     error: true
                // });
            });
    }

    handlePostClick(id) {
        // this.props.history.push('/posts/' + id);
        // OR
        // this.props.history.push({pathname: '/posts/' + id});
    }
    
    render() {
        if(this.state.error)
            return <p className="posts__error">Something went wrong!</p>;

        const posts = this.state.posts.map(post => {
            return (
                <Link 
                    // to={'/posts/' + post.id}
                    to={'/posts/' + post.id}
                    key={post.id}
                    className="posts__link">
                    <Post 
                        postTitle={post.title} 
                        postAuthor={post.author}
                        // passing route-related props
                        // {...this.props}
                        // match={this.props.match}
                        clicked={() => this.handlePostClick(post.id)} />
                </Link>
                // OR + uncomment in handlePostClick
                
                // <Post 
                //     key={post.id}
                //     postTitle={post.title} 
                //     postAuthor={post.author}
                //     clicked={() => this.handlePostClick(post.id)} />
            );
        });

        return (
            <div>
                <section className="posts">
                    {posts}
                </section>
                <Route path={this.props.match.url + '/:id'} exact component={FullPost} />
            </div>
        );
    }
}

export default Posts;