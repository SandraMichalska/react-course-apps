import React, { Component } from 'react';
// import { Route, Link } from 'react-router-dom';
import { Route, NavLink, Switch, Redirect } from 'react-router-dom'; // NavLink for styles

import Posts from './Posts/Posts';
import NewPost from './NewPost/NewPost';
import './Blog.scss';

class Blog extends Component {
    state = {
        auth: false
    }

    render () {
        return (
            <div>
                <header>
                    <nav className="nav">
                        <ul className="nav__list">
                            <li className="nav__item">
                                <NavLink // instead of Link
                                    exact
                                    className="nav__link" // still works...
                                    activeClassName="nav__link--active" // change the default .active
                                    activeStyle={{
                                        textDecoration: 'underline'
                                    }} // inline styles
                                    to="/posts/">Posts
                                </NavLink>
                            </li>
                            <li className="nav__item">
                                <NavLink 
                                    activeClassName="nav__link--active" 
                                    className="nav__link"
                                    to={{
                                        pathname: '/new-post', // absolute path
                                        // pathname: this.props.match.url + '/new-post', // relative path, won't work here
                                        hash: '#sth',
                                        search: '?sth=true'
                                    }}>New post
                                </NavLink>
                            </li>
                        </ul>
                    </nav>
                </header>

                {/* load the first route found */}
                <Switch>
                    {/* Guard */}
                    {this.state.auth ? <Route path="/new-post" component={NewPost} /> : null}
                    <Route path="/posts" component={Posts} />
                    {/* <Route render={() => <h1>Page not found</h1>} /> {/*THIS OR.../*} */}
                    {/* <Route path="/" component={Posts} /> */}
                    <Redirect from="/" to="/posts" /> {/*...THAT/*}
                    {/* <Route path="/posts/:id" exact component={FullPost} /> with /posts */}
                </Switch>
            </div>
        );
    }
}

export default Blog;