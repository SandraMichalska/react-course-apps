import React from 'react';
import './Post.scss';
import { withRouter } from 'react-router-dom';

const Post = (props) => {
    console.log(props);

    return (
        <article 
            className="post"
            onClick={props.clicked}>
            <h1>{props.postTitle}</h1>
            <div className="info">
                <div className="author">{props.postAuthor}</div>
            </div>
        </article>
    );
};

export default withRouter(Post); // to access parent route-related props