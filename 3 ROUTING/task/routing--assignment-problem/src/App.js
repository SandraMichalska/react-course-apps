import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import CourseApp from './components/CourseApp/CourseApp';

class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <div className="App">
          <CourseApp />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
