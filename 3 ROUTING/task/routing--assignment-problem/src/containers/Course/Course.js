import React, { Component } from 'react';

class Course extends Component {
    state = {
        queryParam: null
    };

    componentDidMount() {
        this.getQueryParam();
    }      

    componentDidUpdate() {
        this.getQueryParam();
    }

    getQueryParam = () => {
        const query = new URLSearchParams(this.props.location.search);
        
        for (let param of query.entries()) {
            // console.log(param); // yields ['key', 'value']

            if(this.state.queryParam !== param[1]) {
                this.setState({
                    queryParam: param[1]
                })
            }
        }
    }

    render () {
        return (
            <div>
                <h1>{this.state.queryParam}</h1>
                {/*passing as a param*/}
                {/* <h1>{this.props.match.params.title}</h1>  */}
                
                <p>You selected the Course with ID: {this.props.match.params.id}</p>
            </div>
        );
    }
}

export default Course;