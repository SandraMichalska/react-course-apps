import React from 'react';
import './Navigation.scss';
import { NavLink } from 'react-router-dom';

const Navigation = (props) => {
    return ( 
        <nav className="nav">
            <ul className="nav__list">
                <li className="nav__item">
                    <NavLink 
                        className="nav__link"
                        activeClassName="nav__link--active"
                        activeStyle={{
                            textDecoration: 'underline'
                        }}
                        to="/users">Users
                    </NavLink></li>
                <li className="nav__item">
                    <NavLink 
                        className="nav__link" 
                        activeClassName="nav__link--active"
                        activeStyle={{
                            textDecoration: 'underline'
                        }}
                        to="/courses">Courses
                    </NavLink></li>
            </ul>
        </nav>
    );
}
 
export default Navigation;