import React from 'react';
import Courses from '../../containers/Courses/Courses';
import Users from '../../containers/Users/Users';
import Navigation from '../../components/Navigation/Navigation';
import { Route, Switch, Redirect } from 'react-router-dom';

const CourseApp = () => {
    return (
        <div> 
            <Navigation />
            <Switch>
                <Route path="/users" component={Users} />
                <Route path="/courses" component={Courses} />
                <Redirect from="/all-courses" to="/courses" />
                {/* <Redirect from="/" to="/users" /> */}
                <Route render={() => <p>Page not found</p>} /> 
            </Switch>
        </div>
     );
}
 
export default CourseApp;

