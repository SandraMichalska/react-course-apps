import React, { Component } from 'react';

import './NewPost.scss';
import axios from 'axios';

class NewPost extends Component {
    state = {
        title: '',
        content: '',
        author: 'Max',
        error: false
    }

    handlePostData = () => {
        const postData = {
            title: this.state.title,
            content: this.state.content,
            author: this.state.author
        }

        axios.post('/posts', postData)
            .then(response => {
                console.log(response);
            }).catch(error => {
                console.log(error);
                
                this.setState({
                    error: true
                });
            });
    }

    render () {
        if(this.state.error)
            return <p className="new-post__error">Something went wrong! Cannot send the data.</p>;

        return (
            <div className="new-post">
                <h1>Add a Post</h1>
                <label className="new-post__label">Title</label>
                <input className="new-post__input" type="text" value={this.state.title} onChange={(event) => this.setState({title: event.target.value})} />
                <label className="new-post__label">Content</label>
                <textarea className="new-post__text-area" rows="4" value={this.state.content} onChange={(event) => this.setState({content: event.target.value})} />
                <label className="new-post__label">Author</label>
                <select className="new-post__select" value={this.state.author} onChange={(event) => this.setState({author: event.target.value})}>
                    <option value="Max">Max</option>
                    <option value="Manu">Manu</option>
                </select>
                <button 
                    className="new-post__add-btn"
                    onClick={this.handlePostData}>Add Post</button>
            </div>
        );
    }
}

export default NewPost;