import React from 'react';

import './Post.scss';

const Post = (props) => (
    <article 
        className="post"
        onClick={props.clicked}>
        <h1>{props.postTitle}</h1>
        <div className="info">
            <div className="author">{props.postAuthor}</div>
        </div>
    </article>
);

export default Post;