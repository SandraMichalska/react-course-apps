import React, { Component } from 'react';
import axios from 'axios';

import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import NewPost from '../../components/NewPost/NewPost';
import './Blog.scss';

class Blog extends Component {
    state = {
        posts: [],
        selectedPostId: null,
        error: false
    };

    componentDidMount() {
        axios.get('/posts')
            .then(response => {
                const somePosts = response.data.slice(0, 6);
                const updatedPosts = somePosts.map(post => {
                    return {
                        ...post,
                        author: "Sandra"
                    }
                });

                this.setState({
                    posts: updatedPosts
                })
            }).catch(error => {
                console.log(error);
                
                this.setState({
                    error: true
                });
            });
    }

    handlePostClick(id) {
        this.setState({
            selectedPostId: id
        });
    }

    render () {
        if(this.state.error)
            return <p className="full-post__error">Something went wrong!</p>;

        const posts = this.state.posts.map(post => {
            return <Post 
                key={post.id} 
                postTitle={post.title} 
                postAuthor={post.author}
                postId={post.id}
                clicked={() => this.handlePostClick(post.id)} />;
        });

        return (
            <div>
                <section className="posts">
                    {posts}
                </section>
                <section>
                    <FullPost selectedPostId={this.state.selectedPostId} />
                </section>
                <section>
                    <NewPost />
                </section>
            </div>
        );
    }
}

export default Blog;