import React from 'react';

import classes from './Cockpit.css';

class Cockpit extends React.Component {

    constructor( props ) {
        super( props );
        console.log( '[Cockpit.js] constructor');
    }

    componentWillMount () {
        console.log( '[Cockpit.js] componentWillMount()');
    }

    componentDidMount () {
        console.log( '[Cockpit.js] componentDidMount()');
    }

    componentWillReceiveProps ( nextProps ) {
        console.log( '[Cockpit.js UPDATE] componentWillReceiveProps');
    }

    shouldComponentUpdate ( nextProps, nextState ) {
        console.log( '[Cockpit.js UPDATE] shouldComponentUpdate');
        //return nextProps.persons !== this.props.persons ||
          //  nextProps.changed !== this.props.changed ||
            //nextProps.clicked !== this.props.clicked;
        return true;
    }

    componentWillUpdate ( nextProps, nextState ) {
        console.log( '[Cockpit.js UPDATE] componentWillUpdate');
    }

    componentDidUpdate () {
        console.log( '[Cockpit.js UPDATE] componentDidUpdate');
    }

    render () {
        console.log( '[Cockpit.js] render()' );

	    const assignedClasses = [];
	    let btnClass = '';
	    if (this.props.showPersons) {
		btnClass = classes.Red;
	    }

	    if ( this.props.persons.length <= 2 ) {
	      assignedClasses.push( classes.red ); // classes = ['red']
	    }
	    if ( this.props.persons.length <= 1 ) {
	      assignedClasses.push( classes.bold ); // classes = ['red', 'bold']
	    }

	    return (
		<div className={classes.Cockpit}>
		    <h1>{ this.props.appTitle }</h1>
		    <p className={assignedClasses.join( ' ' )}>This is really working!</p>
		    <button
		        className={btnClass}
		        onClick={this.props.clicked}>Toggle Persons</button>
		</div>
	    );
	}
};

export default Cockpit;
