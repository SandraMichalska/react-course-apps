import React, { Component } from 'react';

import Person from './Person/Person';

class Persons extends Component {
    constructor( props ) {
        super( props );
        console.log( '[People.js] Constructor');
    }

    componentWillMount () {
        console.log( '[People.js] componentWillMount()');
    }

    componentDidMount () {
        console.log( '[People.js] componentDidMount()');
    }

    componentWillReceiveProps ( nextProps ) {
        console.log( '[People.js UPDATE] componentWillReceiveProps');
    }

    shouldComponentUpdate ( nextProps, nextState ) {
        console.log( '[People.js UPDATE] shouldComponentUpdate');
        //return nextProps.persons !== this.props.persons ||
          //  nextProps.changed !== this.props.changed ||
            //nextProps.clicked !== this.props.clicked;
        return true;
    }

    componentWillUpdate ( nextProps, nextState ) {
        console.log( '[People.js UPDATE] componentWillUpdate');
    }

    componentDidUpdate () {
        console.log( '[People.js UPDATE] componentDidUpdate' );
    }

    render () {
        console.log( '[People.js] render()' );
        return this.props.persons.map( ( person, index ) => {
            return <Person
                click={() => this.props.clicked( index )}
                name={person.name}
                age={person.age}
                key={person.id}
                changed={( event ) => this.props.changed( event, person.id )} />
        } );
    }
}

export default Persons;
