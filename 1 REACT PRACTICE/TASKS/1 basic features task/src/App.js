// container holding state, class component

import React, { Component } from 'react';
import './App.css';

import UserInput from './User/UserInput/UserInput';
import UserOutput from './User/UserOutput/UserOutput';

class App extends Component {
  // the normal way
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     name: "Sandra"
  //   }
  // }

  // the ES6 class properties way
  state = {
    userName: "Sandra"
  }

  // use an arrow fct to avoid binding this ;)
  // handleNameChange(e) {
  //   this.setState({
  //     userName: e.target.value
  //   });
  // }

  handleNameChange = e => {
    this.setState({
      userName: e.target.value
    });
  }

  render() {
    return (
      <div className="App">
        {/*<UserInput name={this.state.userName} onNameChange={this.handleNameChange.bind(this)} />*/}
        <UserInput
          currentName={this.state.userName}
          onNameChange={this.handleNameChange} /> {/* just pass a reference, without ()! */}
        <UserOutput userName={this.state.userName} />
        <UserOutput userName="Some hardcoded name" />
        <UserOutput userName="Some hardcoded name 2" />
      </div>
    );
  }
}

export default App;
