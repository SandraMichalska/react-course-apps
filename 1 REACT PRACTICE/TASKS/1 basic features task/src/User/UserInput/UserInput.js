// no state, just a functional component
import React from 'react';

const UserInput = (props) => {
    // inline styles, only for this component
    const style = {
        fontSize: '2em',
        border: '1px solid black'
    };

    return <input  
            //onChange={(e) => props.onNameChange(e.target.value)} // e is passed automatically!!!
            type="text"
            onChange={props.onNameChange} // when we type sth, the state userName changes so the value below too!
            //placeholder={props.currentName} // use value instead!
            value={props.currentName} // the initial value passed; two-way binding!
            style={style} />
};

export default UserInput;