// no state, just a functional component
import React from 'react';
// external stylesheet; found and bundled by webpack
import './UserOutput.css';

const UserOutput = (props) => (
    <div className="user-output">
        <p className="user-output__txt">Text 1</p>
        <p className="user-output__txt">Name: {props.userName}</p>
    </div>
);

export default UserOutput;