How to use the downloaded files

1) Run "npm install" in the extracted folder
2) Run "npm start" to view the project

Compare my code to yours to solve issues you might encounter. You may also copy the content of src/ into your own project's src/ folder to use your project setup (which might've been created with a different create-react-app version) and still use my code.


Create an input field (in App component) with a change listener which outputs the length of the entered text below it (e.g. in a paragraph).

Create a new component (=> ValidationComponent) which receives the text length as a prop

Inside the ValidationComponent, either output "Text too short" or "Text long enough" depending on the text length (e.g. take 5 as a minimum length)

Create another component (=> CharComponent) and style it as an inline box (=> display: inline-block, padding: 16px, text-align: center, margin: 16px, border: 1px solid black).

Render a list of CharComponents where each CharComponent receives a different letter of the entered text (in the initial input field) as a prop.

When you click a CharComponent, it should be removed from the entered text. + set up a two-way binding with the input


Hint: Keep in mind that JavaScript strings are similary to arrays (but map, slice etc. are not available there)