import React from 'react';

const Validation = (props) => {
    const message = props.inputLength < 5 ? "Text too short" : "Text long enough";
    // OR
    // let validationMessage = 'Text long enough';

    // if (props.inputLength <= 5) {
    //     validationMessage = 'Text too short';
    // }

    return <p>{message}</p>
}

export default Validation;