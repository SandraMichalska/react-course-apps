import React, { Component } from 'react';
import './App.css';
import Validation from './Validation/Validation';
import Char from './Char/Char';

class App extends Component {
  state = {
    inputText: ""
  }

  handleInputChange = (e) => {
    this.setState({
      inputText: e.target.value
    });
  }

  handleCharClick = (index) => {
    const inputText = this.state.inputText;
    const inputTextArr = inputText.split('');
    inputTextArr.splice(index, 1);
    const updatedText =  inputTextArr.join('');

    this.setState({
      inputText: updatedText
    });
  }

  render() {
    let chars = null;

    if(this.state.inputText) {
      const inputText = this.state.inputText;
      const inputTextArr = inputText.split('');
  
      chars = (
        inputTextArr.map((char, index) => {
          return <Char
            char={char}
            key={index}
            onCharClick={() => this.handleCharClick(index)} />;
        })
      );
    }
    //OR just return [] from map;)
    // const charList = this.state.userInput.split('').map((ch, index) => {
    //   console.log('fsdds');
    //         return <Char 
    //           character={ch} 
    //           key={index}
    //           clicked={() => this.deleteCharHandler(index)} />;
    //       });

    return (
      <div className="App">
        <input type="text" onChange={this.handleInputChange} value={this.state.inputText}/>
        <p>Input length: {this.state.inputText.length}</p>
        <p>Text: {this.state.inputText}</p>

        <Validation inputLength={this.state.inputText.length}/>
        {chars}
      </div>
    );
  }
}

export default App;
