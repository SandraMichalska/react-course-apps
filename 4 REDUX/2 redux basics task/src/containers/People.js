import React, { Component } from 'react';
import { connect } from 'react-redux';

import Person from '../components/Person/Person';
import AddPerson from '../components/AddPerson/AddPerson';
import * as actionTypes from '../store/actions';

class People extends Component {
    // state = {
    //     people: []
    // }




// a solution with unnecessary click handlers ;)
    // handleAddPerson = () => {
    //     const newPerson = {
    //         id: Math.random(), // not really unique but good enough here!
    //         name: 'Max',
    //         age: Math.floor( Math.random() * 40 )
    //     }

    //     this.props.onAddPerson(newPerson);
    //     // this.setState( ( prevState ) => {
    //     //     return { people: prevState.people.concat(newPerson)}
    //     // } );
    // }

    // handleDeletePerson = (personId) => {
    //     this.props.onDeletePerson(personId);
    //     // this.setState( ( prevState ) => {
    //     //     return { people: prevState.people.filter(person => person.id !== personId)}
    //     // } );
    // }

    render () {
        return (
            <div>
                <AddPerson personAdded={this.props.onAddPerson} />
                {/* <AddPerson personAdded={this.handleAddPerson} /> */}
                {this.props.people.map(person => (
                    <Person 
                        key={person.id}
                        name={person.name} 
                        age={person.age} 
                        clicked={() => this.props.onDeletePerson(person.id)}/>
                        // clicked={() => this.handleDeletePerson(person.id)}/>
                ))}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        people: state.peopleRed.people
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onAddPerson: () => dispatch({type: actionTypes.ADD_PERSON}),
        onDeletePerson: (personId) => dispatch({type: actionTypes.DELETE_PERSON, personId: personId })
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(People);