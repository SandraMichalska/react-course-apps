import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

import './index.css';
import App from './App';
import peopleReducer from './store/reducers/people';
import reducer2 from './store/reducers/reducer2'; // dummy
import registerServiceWorker from './registerServiceWorker';

const reducers = combineReducers({
    peopleRed: peopleReducer,
    red2: reducer2
});

const store = createStore(reducers);

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
