const redux = require('redux');
const createStore = redux.createStore;

const initialState = {
    counter: 0
};

//REDUCER
const rootReducer = (state = initialState, action) => {
    if(action.type === 'INC_COUNTER') {
        return {
            ...state,
            counter: state.counter + 1 // state = old state passed here!
        }
    }

    if(action.type === 'ADD_COUNTER') {
        return {
            ...state,
            counter: state.counter + action.value // value by which to increment = value passed by the action
        }
    }

    return state; // default
};

//STORE
const store = createStore(rootReducer);

console.log('after creating store', store.getState());


//SUBSCRIPTION
store.subscribe(() => {
    console.log('subscription!', store.getState().counter);
});


//DISPATCH ACTIONS
store.dispatch({type: 'INC_COUNTER'});
console.log('after dispatch 1', store.getState());
store.dispatch({type: 'ADD_COUNTER', value: 10});
console.log('after dispatch 2', store.getState());
